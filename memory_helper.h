#pragma once

//
// Created by Bittner on 19.12.2019.
//

#include <cmath>

namespace frg
{    

template<typename T>
struct alignas(T) uninitialized_t
{
	char _[sizeof(T)];
};

inline void* aligned_alloc(size_t alignment, size_t size) {
#ifdef WIN32
    return _aligned_malloc(size, alignment);
#elif defined(__APPLE__)
	//on apple, alignment needs to be a multiple of sizeof(void*) = 8
	// if (alignment < sizeof(void*))
	return malloc(size);
	// else
	// 	return ::aligned_alloc(alignment, size);
#else
    return ::aligned_alloc(alignment, size);
#endif
}

template<typename T>
constexpr size_t element_size()
{
	return std::max(sizeof(T), alignof(T));
}

template<typename T>
constexpr size_t max_elements(size_t bytes)
{
	return std::max(size_t(1), bytes / element_size<T>());
}

/* template<typename T> */
/* constexpr T* compute_capacity(size_t buffer_size_in_bytes, T *first_address) */
/* { */

/* } */

template<typename T>
T* allocate_elements(size_t count)
{
	return (T*)aligned_alloc(alignof(T), element_size<T>() * count);
}

inline void aligned_free(void* data) {
#ifdef WIN32
    _aligned_free(data);
#else
    free(data);
#endif
}
}
