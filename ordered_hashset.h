#pragma once
#include "hashmap.h"
#include "list.h"

template<typename Key, int smallsize=4>
struct ordered_hashset
{
	void insert(const Key &key)
	{
		set.insert(key, list.size());
		list.add(key);
	}

	bool contains(const Key &key) const
	{
		return set.contains(key);
	}

	Array<Key>   list;
	hashmap<Key, uint32_t> set;
};
