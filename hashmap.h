#pragma once

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include "hash.h"

#include "memory_helper.h"

template<typename Key, typename Value, size_t smallsize = 4>
class hashmap
{
public:
	struct iterator
	{
		iterator operator++() const {
			auto pos_ = pos;
			while (++pos_ < map.capacity_ && map.filled_[pos_] != FILLED);
			return iterator{pos_, map};
		}

		std::tuple<Key&, Value&> operator*()
		{
			return { map.keys_[pos], map.data_[pos] };
		}

		bool operator!=(iterator &other) const
		{
			return pos != other.pos;
		}

		bool filled() const
		{
			return map.filled_[pos] == FILLED;
		}

		size_t pos{0};
		hashmap &map;
	};

	struct const_iterator
	{
		const_iterator operator++() const {
			auto pos_ = pos;
			while (++pos_ < map.capacity_ && map.filled_[pos_] != FILLED);
			return const_iterator{pos_, map};
		}

		std::tuple<Key&, Value&> operator*()
		{
			return { map.keys_[pos], map.data_[pos] };
		}

		bool operator!=(const_iterator &other) const
		{
			return pos != other.pos;
		}

		bool filled() const
		{
			return map.filled_[pos] == FILLED;
		}

		size_t pos{0};
		const hashmap &map;
	};

	hashmap() :
	    filled_(smallsize > 0 ? filled_buffer_ : nullptr),
	    data_(smallsize > 0 ? (Value *)data_buffer_ : nullptr),
	    keys_(smallsize > 0 ? (Key *)keys_buffer_ : nullptr),
		capacity_(smallsize)
	{
		if (filled_) memset(filled_, 0, capacity_ * sizeof(Status));
	}

	~hashmap()
	{
		if (data_ && !is_small()) {
			free_and_destruct_();
		}
	}

	void clear()
	{
		*this = hashmap();
	}

	iterator begin()
	{
		iterator ret{0, *this};
		if (ret.filled())
			return ret;
		else
			return ++ret;
	}

	iterator end() { return iterator{capacity_, *this}; }

	const_iterator begin() const
	{
		const_iterator ret{0, *this};
		if (ret.filled())
			return ret;
		else
			return ++ret;
	}

	const_iterator end() const { return const_iterator{capacity_, *this}; }

	hashmap(const hashmap &other) :
	    size_(other.size_),
	    capacity_(other.capacity_)
	{
		if (capacity_ > smallsize) {
			filled_ = (Status *)malloc(capacity_ * sizeof(Status));
			data_   = frg::allocate_elements<Value>(capacity_);
			keys_   = frg::allocate_elements<Key>(capacity_);
		} else {
			filled_ = filled_buffer_;
			data_   = (Value *)data_buffer_;
			keys_   = (Key *)keys_buffer_;
		}

		memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
		copy_(other);
	}

	hashmap(hashmap &&other) :
	    size_(other.size_),
	    capacity_(other.capacity_),
	    filled_(other.filled_),
	    data_(other.data_),
	    keys_(other.keys_)
	{
		if (other.is_small()) {
			data_   = (Value *)data_buffer_;
			keys_   = (Key *)keys_buffer_;
			filled_ = filled_buffer_;
			memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
			move_(std::move(other));
		}

		other.data_   = nullptr;
		other.filled_ = nullptr;
		other.keys_   = nullptr;
	}

	void move_(hashmap &&other)
	{
		for (size_t i = 0; i < capacity_; ++i) {
			if (filled_[i] == FILLED) {
				new (&keys_[i]) Key(std::move(other.keys_[i]));
				new (&data_[i]) Value(std::move(other.data_[i]));
			}
		}
	}

	void copy_(const hashmap &other)
	{
		for (size_t i = 0; i < capacity_; ++i) {
			if (filled_[i] == FILLED) {
				new (&keys_[i]) Key(other.keys_[i]);
				new (&data_[i]) Value(other.data_[i]);
			}
		}
	}

	void free_and_destruct_()
	{
		for (size_t i = 0; i < capacity_; ++i) {
			if (filled_[i] == FILLED) {
				keys_[i].~Key();
				data_[i].~Value();
			}
		}
		free(filled_);
		frg::aligned_free(data_);
		frg::aligned_free(keys_);
		filled_   = nullptr;
		data_     = nullptr;
		keys_     = nullptr;
		size_     = 0;
		capacity_ = 0;
	}

	hashmap &operator=(const hashmap &other)
	{
		this->~hashmap();

		size_     = other.size_;
		capacity_ = other.capacity_;

		if (capacity_ > smallsize) {
			filled_ = (Status *)malloc(capacity_ * sizeof(Status));
			data_   = frg::allocate_elements<Value>(capacity_);
			keys_   = frg::allocate_elements<Key>(capacity_);
		} else {
			filled_ = filled_buffer_;
			data_   = (Value *)data_buffer_;
			keys_   = (Key *)keys_buffer_;
		}

		memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
		copy_(other);

		return *this;
	}

	hashmap &operator=(hashmap &&other)
	{
		size_     = other.size_;
		capacity_ = other.capacity_;
		filled_   = other.filled_;
		data_     = other.data_;
		keys_     = other.keys_;

		if (other.is_small()) {
			data_   = (Value *)data_buffer_;
			filled_ = (Status *)filled_buffer_;
			keys_   = (Key *)keys_buffer_;
			memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
			move_(std::move(other));
		}

		other.data_   = nullptr;
		other.keys_   = nullptr;
		other.filled_ = nullptr;

		return *this;
	}

	void insert(const Key &k, Value v)
	{
		if (size_ >= capacity_ / 2) {
			if (!capacity_)
				reserve(4);
			else
				reserve(capacity_ * 2);
		}
		auto idx = find(k);
		if (idx == ~0ul) {
			idx = find_empty(k);
			++size_;
		}

		assert(idx != ~0ul);
		
		new (&data_[idx]) Value(std::move(v));
		new (&keys_[idx]) Key(k);

		filled_[idx] = FILLED;

	}

	size_t size() const { return size_; }

	bool contains(const Key &k) const
	{
		return find(k) != ~0ul;
	}

	void remove(const Key &k)
	{
		auto idx     = find(k);
		filled_[idx] = TOMBSTONE;
		--size_;
		if (size_ < capacity_ / 2) {
			reserve(capacity_ / 2);
		}
	}

	Value &operator[](const Key &key)
	{
		auto idx = find(key);
		if (idx == ~0ul) {
			fputs("KeyError\n", stderr);
			assert(false);
			exit(EXIT_FAILURE);
		}

		return data_[idx];
	}

	const Value &operator[](const Key &key) const
	{
		auto idx = find(key);
		if (idx == ~0ul) {
			fprintf(stderr, "KeyError\n");
			assert(false);
			exit(EXIT_FAILURE);
		}
		
		return data_[idx];
	}

private:
	enum Status
	{
		EMPTY,
		TOMBSTONE,
		FILLED
	};

	bool is_small()
	{
		return smallsize && data_ >= (Value *)&data_buffer_[0] &&
				data_ < (Value *)&data_buffer_[smallsize];
	}

	size_t find(const Key &k) const
	{
		if (!capacity_) return ~0ul;
		size_t idx = get_idx(k);

		// linear probing
		while ((filled_[idx] == FILLED && keys_[idx] != k) ||
		       filled_[idx] == TOMBSTONE)
			idx = (++idx) % capacity_;

		if (filled_[idx] == FILLED && keys_[idx] == k) return idx;
		return ~0ul;
	}

	size_t get_idx(const Key &k) const
	{
		return frg_hash::hash<Key>::compute_hash(k) % capacity_;
	}

	size_t find_empty(const Key &k) const
	{
		size_t idx = get_idx(k);

		// linear probing
		while (filled_[idx] == FILLED) idx = (++idx) % capacity_;

		return idx;
	}

	void reserve(size_t newcapacity)
	{
		if (newcapacity <= std::max(capacity_, smallsize)) return;

		Value * newdata_  = nullptr;
		Key *   newkeys_  = nullptr;
		Status *newfilled = nullptr;
		if (newcapacity > smallsize) {
			newfilled = (Status *)malloc(newcapacity * sizeof(Status));
			newdata_  = frg::allocate_elements<Value>(newcapacity);
			newkeys_  = frg::allocate_elements<Key>(newcapacity);
		} else {
			newdata_  = (Value *)data_buffer_;
			newkeys_  = (Key *)keys_buffer_;
			newfilled = filled_buffer_;
		}
		memset(newfilled, 0, newcapacity * sizeof(Status));

		Value * olddata_  = data_;
		Key *   oldkeys_  = keys_;
		Status *oldfilled = filled_;

		data_   = newdata_;
		keys_   = newkeys_;
		filled_ = newfilled;

		size_            = 0;
		auto oldcapacity = capacity_;
		capacity_        = newcapacity;
		for (size_t i = 0; i < oldcapacity; ++i) {
			if (oldfilled[i] == FILLED)
				insert(oldkeys_[i], std::move(olddata_[i]));
		}

		if (oldcapacity > smallsize) {
			data_     = olddata_;
			keys_     = oldkeys_;
			filled_   = oldfilled;
			capacity_ = oldcapacity;
			free_and_destruct_();
			capacity_ = newcapacity;
		}
		capacity_ = newcapacity;
		data_     = newdata_;
		keys_     = newkeys_;
		filled_   = newfilled;
	}

	frg::uninitialized_t<Value> data_buffer_[smallsize];
	frg::uninitialized_t<Key>   keys_buffer_[smallsize];
	Status *                    filled_{nullptr};
	Value *                     data_{nullptr};
	Key *                       keys_{nullptr};
	size_t                      size_{0};
	size_t                      capacity_{smallsize};
	Status                      filled_buffer_[smallsize];
};
