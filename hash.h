#pragma once

#include <cstring>
#include <cstdint>

namespace frg_hash
{
inline uint32_t xorshift32(uint32_t x)
{
	x += 1;  // xorshift(0) == 0
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return x;
}

inline uint64_t xorshift64(uint64_t x)
{
	x += 1;  // xorshift(0) == 0
	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;
	return x;
}

template<typename T>
struct hash;

template<>
struct hash<uint8_t>
{
	static uint32_t compute_hash(uint8_t value)
	{
		return frg_hash::xorshift32(uint32_t(value));
	}
};

template<>
struct hash<uint32_t>
{
	static uint32_t compute_hash(uint64_t value)
	{
		return frg_hash::xorshift32(uint32_t(value));
	}
};

template<>
struct hash<uint64_t>
{
	static uint64_t compute_hash(uint64_t value)
	{
		return frg_hash::xorshift64(value);
	}
};

template<>
struct hash<const char*>
{
	static uint64_t compute_hash(const char* str)
	{
		uint64_t *buffer = (uint64_t*)str;
		size_t len = strlen(str);
		size_t len_buffer = len / sizeof(uint64_t);

		uint64_t ret{0};
		for (size_t i = 0; i < len_buffer; ++i) {
			ret ^= hash<uint64_t>::compute_hash(buffer[i]);
		}
		//hash remaining bytes
		for (size_t i = len_buffer * sizeof(uint64_t); i < len; ++i) {
			ret ^= hash<uint8_t>::compute_hash(str[i]);
		}
		return ret;
	}
};
}
