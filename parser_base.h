#pragma once

#include "str.h"
#include "ordered_hashmap.h"
#include "hashset.h"

#include <string>
#include <string_view>
#include <vector>

namespace cfg
{

struct Module
{
	hashmap<String, String> aliases;
	String                  filename;
	String                  module_prefix;
	uint32_t                nodeIdx;
};

struct Parser;
struct ParserState
{
	Parser *   parser;
	uint32_t   idx;
	uint32_t   prev_state_idx;
	uint32_t   moduleIdx;
	String     filename;
	StringView str;
	uint32_t   line;
	uint32_t   col;
	int32_t    i;
};

struct Token
{
	StringView str;

	enum class Type
	{
		None,
		OpenSquareBracket,
		CloseSquareBracket,
		OpenCurlyBracket,
		CloseCurlyBracket,
		OpenAngleBracket,
		CloseAngleBracket,
		Comma,
		Equal,
		Colon,
		Semicolon,
		Number,
		Name,
		Dot,
		Bar
	} type{Type::None};
};

struct KeyValuePair
{
	String key;
	ParserState value_parser;
	String value;
	Array<Token> value_tokens;
};

inline const char* token_type_to_str(Token::Type type)
{
	switch (type)  {
	case Token::Type::None:
		return "None";
	case Token::Type::OpenSquareBracket:
		return "OpenSquareBracket";
	case Token::Type::CloseSquareBracket:
		return "CloseSquareBracket";
	case Token::Type::OpenCurlyBracket:
		return "OpenCurlyBracket";
	case Token::Type::CloseCurlyBracket:
		return "CloseCurlyBracket";
	case Token::Type::OpenAngleBracket:
		return "OpenAngleBracket";
	case Token::Type::CloseAngleBracket:
		return "CloseAngleBracket";
	case Token::Type::Comma:
		return "Comma";
	case Token::Type::Equal:
		return "Equal";
	case Token::Type::Colon:
		return "Colon";
	case Token::Type::Semicolon:
		return "Semicolon";
	case Token::Type::Number:
		return "Number";
	case Token::Type::Name:
		return "Name";
	case Token::Type::Dot:
		return "Dot";
	case Token::Type::Bar:
		return "Bar";
	default:
		return "";
	}
}

struct Node
{
	ParserState                           parser_state;
	String                                name;
	String                                type_string;
	String                                module_prefix;
	ordered_hashmap<String, KeyValuePair> settings;
	uint32_t                              moduleIdx;
	uint32_t                              type;
	bool                                  is_override;
};

struct Parser
{
	void parse_file(String filename);

	hashset<String>           nodeSet;
	hashmap<String, uint32_t> nodeMap;
	hashmap<String, String>   global_aliases;
	Array<ParserState>        parser_states;
	Array<Node>               nodes;
	Array<Module>             modules;
	Array<String>             search_paths;
};

bool       is_relative_path(StringView filepath);
StringView extract_folder(StringView filepath);

enum class ParserMessageSeverity
{
	Warning,
	Error
};

String report_parser_error(
    const ParserState &         state,
    const String                msg,
    const ParserMessageSeverity severity = ParserMessageSeverity::Error);

void             report_error_and_exit(const String &str);
ParserState &    advance(ParserState &state);

bool  parse_bool(StringView val);
int   parse_int(StringView val);
float parse_float(StringView val);

bool             consume_comment(ParserState &state);
bool             consume_whitespace(ParserState &state);
void             consume_all_ignorable(ParserState &state);
Token            parse_next_token(ParserState &state);
Token            peek_next_token(ParserState state);
void             skip_next_token(ParserState &state);
void             assert_token_type(ParserState &state,
                                   const Token &token,
                                   Token::Type  type);

StringView strip_end(StringView v);
StringView parse_until(ParserState &state, char c);
StringView parse_until_space(ParserState &state);
void       parse_key_values(ParserState &state, Node &node);
void       init_kv_parser(KeyValuePair &kv);

StringView get_module_node_name(StringView name);
uint32_t   get_module_depth(const String &name);
StringView strip_first_module_name(StringView name);
StringView strip_module_names(const String &name);

}  // namespace cfg

#ifdef PARSER_BASE_IMPL

#include <cctype>
#include <string>
#include <fstream>
#include <cassert>
#include "parser_base.h"

namespace cfg
{
StringView extract_folder(StringView filepath)
{
	size_t i = filepath.size();
	for (; i > 0; --i) {
		if (filepath.begin()[i - 1] == '/') break;
	}
	return StringView(filepath.begin(), i);
}

bool is_relative_path(StringView filepath)
{
	return filepath.begin()[0] != '/';
}

ParserState& advance(ParserState &state)
{
	++state.col;
	if (state.i < state.str.size() && state.str[state.i] == '\n') {
		state.col = 1;
		++state.line;
	}
	++state.i;

	return state;
}

uint32_t seek_back(ParserState &state, char c)
{
	uint32_t i = state.i;
	for (; i > 0 && state.str[i] != c; --i);
	return i;
}

ParserState& step_back(ParserState &state)
{
	--state.col;
	if (state.str[state.i] == '\n') {
		state.col = state.i - seek_back(state, '\n');
		--state.line;
	}
	--state.i;

	return state;
}

const char* severity_to_str(const ParserMessageSeverity severity)
{
	switch(severity) {
	case ParserMessageSeverity::Error:
		return "Error";
	case ParserMessageSeverity::Warning:
		return "Warning";
	default:
		return "";
	}
}

String report_parser_error(const ParserState &         state,
                           const String                msg,
                           const ParserMessageSeverity severity)
{
	StringBuilder module_msg_builder;

	uint32_t prev = state.prev_state_idx;
	uint32_t cur  = state.idx;
	while (prev != ~0u && !state.parser->parser_states[prev].filename.empty()) {
		const ParserState &ps     = state.parser->parser_states[prev];
		const Module &module = state.parser->modules[state.moduleIdx];
		cur                  = ps.idx;
		const ParserState &ps_node_copy =
		    state.parser->nodes[module.nodeIdx].parser_state;
		String msg;
		msg.resize(256);
		msg.format("module (%s) included at: %s:%i:%i\n",
				   module.module_prefix.begin(),
		           ps_node_copy.filename.begin(),
		           ps_node_copy.line,
		           ps_node_copy.col);
		module_msg_builder.add(msg);
		prev = ps.prev_state_idx;
	}

	String module_msg = module_msg_builder.build();
	String ret;
	ret.resize(256);
	ret.format("\n%s[%s:%i:%i] %s: %s",
	           module_msg.begin(),
	           state.filename.begin(),
	           state.line,
	           state.col,
	           severity_to_str(severity),
	           msg.begin());
	return ret;
}

void report_error_and_exit(const String &str)
{
	printf("%s\n", str.begin());
	assert(false);
	exit(EXIT_FAILURE);
}

void assert_token_type(ParserState &state, const Token &token, Token::Type type)
{
	if (token.type != type) {
		String token_str = token_type_to_str(token.type);
		if (token.type == Token::Type::Name || token.type == Token::Type::Number) {
			token_str = String().format("\"%s\"", token.str.begin());
		}
		report_error_and_exit(report_parser_error(
		    state,
		    String().format("ERROR: Unexpected Token. Expected %s, but got %s",
		                    token_type_to_str(type),
		                    token_str.begin())));
	}
}

float parse_float(StringView val)
{
	return float(std::atof(val.begin()));
}

int parse_int(StringView val)
{
	return std::atoi(val.begin());
}

bool parse_bool(StringView val)
{
	if (val == "true") return true;
	return false;
}

bool consume_comment(ParserState &state)
{
	if (state.i >= state.str.size()) return false;
	if (state.str[state.i] != '#') return false;
	while (state.i < state.str.size() && state.str[state.i] != '\n')
		advance(state);
	return true;
}

bool consume_whitespace(ParserState &state)
{
	if (state.i >= state.str.size()) return false;
	if (!isspace(state.str[state.i]) && !iscntrl(state.str[state.i]))
		return false;
	for (; state.i < state.str.size() &&
	       (isspace(state.str[state.i]) || iscntrl(state.str[state.i]));
	     advance(state));

	return true;
}

void consume_all_ignorable(ParserState &state)
{
	assert(state.i < state.str.size());
	while (consume_comment(state) || consume_whitespace(state));
}

Token peek_next_token(ParserState state)
{
	if (state.i >= state.str.size()) return {};
	return parse_next_token(state);
}

void skip_next_token(ParserState &state)
{
	if (state.i >= state.str.size()) return;
	parse_next_token(state);
}

Token::Type most_likely_token_type(ParserState &state, char c, bool token_start)
{
	switch (c) {
	case '[': return Token::Type::OpenSquareBracket;
	case ']': return Token::Type::CloseSquareBracket;
	case '{': return Token::Type::OpenCurlyBracket;
	case '}': return Token::Type::CloseCurlyBracket;
	case '<': return Token::Type::OpenAngleBracket;
	case '>': return Token::Type::CloseAngleBracket;
	case ',': return Token::Type::Comma;
	case '=': return Token::Type::Equal;
	case ':': return Token::Type::Colon;
	case ';': return Token::Type::Semicolon;
	case '.': return Token::Type::Dot;
	case '|': return Token::Type::Bar;
	default:
		if (isdigit(c) || (token_start && c == '-'))
		return Token::Type::Number;
	else
		return Token::Type::Name;
	}
	return Token::Type::None;
}

bool consume_token(ParserState &state, Token &token, bool token_start)
{
	char c = state.str[state.i];
	if (isspace(c) || iscntrl(c)) return true;

	bool token_finished = false;
	auto new_type = most_likely_token_type(state, c, token_start);

	switch (token.type) {
	case Token::Type::None: {
		token.type = new_type;
		break;
	}
	case Token::Type::Name: {
		if (new_type != Token::Type::Name && new_type != Token::Type::Number &&
		    new_type != Token::Type::Dot)
			token_finished = true;
		break;
	}
	case Token::Type::Number: {
		if (c == '-') {
			report_error_and_exit(
			    report_parser_error(state, "Arithmetic is not supported"));
		}
		if (new_type != Token::Type::Number && new_type != Token::Type::Dot)
			token_finished = true;
		break;
	}
	case Token::Type::Dot: {
		if (new_type == Token::Type::Number || new_type == Token::Type::Name) {
			token.type = new_type;
		}
		break;
	}
	case Token::Type::OpenSquareBracket: 
	case Token::Type::CloseSquareBracket:
	case Token::Type::OpenCurlyBracket:
	case Token::Type::CloseCurlyBracket:
	case Token::Type::OpenAngleBracket:
	case Token::Type::CloseAngleBracket:
	case Token::Type::Comma:
	case Token::Type::Equal:
	case Token::Type::Colon:
	case Token::Type::Semicolon:
	case Token::Type::Bar:
		token_finished = true;
		break;
	default:
		report_error_and_exit(report_parser_error(
		    state, String().format("Unexpected character: %c", c)));
	}

	return token_finished;
}

Token parse_next_token(ParserState &state)
{
	if (state.i >= state.str.size()) {
		report_error_and_exit(report_parser_error(state, "Unexpected end of parser input"));
	}
	assert(state.i < state.str.size());
	consume_all_ignorable(state);
	const char *token_start = &state.str[state.i];
	size_t      cnt         = 0;
	int32_t     scope_depth = 0;

	Token ret;
	for (; state.i < state.str.size() && !consume_token(state, ret, cnt == 0);
	     advance(state), ++cnt);

	ret.str = StringView(token_start, cnt);
	return ret;
}

StringView parse_until_space(ParserState &state)
{
	consume_all_ignorable(state);
	const char *token_start = &state.str[state.i];
	size_t      cnt         = 0;
	for (; state.i < state.str.size() && !isspace(state.str[state.i]);
	     advance(state), ++cnt);

	return StringView(token_start, cnt);
}

StringView strip_end(StringView v)
{
	if (!v.size()) return v;
	while (isspace(v[v.size() - 1])) v = StringView(v.begin(), v.size() - 1);
	return v;
}

StringView parse_until(ParserState &state, char c)
{
	size_t      cnt   = 0;
	const char *start = &state.str[state.i];
	for (; state.i < state.str.size() && state.str[state.i] != c;
	     advance(state), ++cnt);
	return StringView(start, cnt);
}

bool is_node_name(const Parser &parser,
                  const String &module_prefix,
                  const String &name)
{
	return parser.nodeSet.contains(
	    (StringBuilder() + module_prefix + name).build());
}

//TODO at some point we have to bite the bullet and work with a full blown AST
String resolve_alias(uint32_t module_idx, StringView name, ParserState &state)
{
	if (name.contains("$")) {
		String prefix;
		String alias_name;
		String suffix;
		size_t slash_pos;
		if (((slash_pos = name.find("/")) < ~0ul)
			&& name.find(":") == ~0ul) {
			if (name.begin()[0] == '$') {
				alias_name = String(StringView(name.begin(), slash_pos));
				suffix     = String(StringView(name.begin() + slash_pos,
                                           name.size() - (slash_pos)));
			} else {
				prefix     = String(StringView(name.begin(), slash_pos + 1));
				alias_name = String(StringView(name.begin() + slash_pos + 1,
				                               name.size() - (slash_pos + 1)));
			}
		}
		else {
			alias_name = String(name);
		}

		String ret;
		if (module_idx < ~0u) {
			const auto &module = state.parser->modules[module_idx];
			if (module.aliases.contains(alias_name)) {
				ret = (StringBuilder() + prefix + module.aliases[alias_name] +
				       suffix)
				          .build();
			}
		}
		if (ret.empty() && peek_next_token(state).type == Token::Type::Colon) {
			skip_next_token(state);
			auto   name_token = parse_next_token(state);
			String fullname   = String(StringView(
                name.begin() + 1, name.size() + name_token.str.size()));

			if (state.parser->nodeMap.contains(fullname)) {
				const auto &node =
				    state.parser->nodes[state.parser->nodeMap[fullname]];
				assert_token_type(
				    state, peek_next_token(state), Token::Type::Colon);
				skip_next_token(state);

				auto key_token = parse_next_token(state);
				assert_token_type(state, key_token, Token::Type::Name);
				String key = String(key_token.str);
				if (node.settings.contains(key)) {
					ret = node.settings.get(key).value;
				}
			}
		}
		if (ret.empty()) {
			String msg;
			const auto &module = state.parser->modules[module_idx];
			if (module_idx < ~0u) {
				msg.format(
				    "could not resolve alias "
				    "%s in module \"%s\" included "
				    "from: \"%s\"\n",
				    alias_name.begin(),
				    module.module_prefix.begin(),
				    module.filename.begin());
			} else {
				msg.format("could not resolve alias %s\n", alias_name.begin());
			}
			printf("%s\n",
			       report_parser_error(
			           state,
			           msg,
			           ParserMessageSeverity::Warning)
			           .begin());
			return "";
		}
		printf("DEBUG: resolved %s to %s\n", alias_name.begin(), ret.begin());
		return ret;
	}
	return String(name);
}

void parse_file_(Parser & parser,
                 uint32_t prev_state_idx,
                 String   filename,
                 uint32_t moduleIdx);

void parse_module_node(Parser &           parser,
                       ParserState &      state,
                       const Node &       node)
{
	const auto &settings = node.settings.list;
	String path;
	for (const auto &kv : node.settings.list) {
		if (kv.key == "path") {
			path = kv.value;
			// if (is_relative_path(kv.value)) {
			// 	path = (StringBuilder() + extract_folder(state.filename) + path)
			// 	           .build();
			// }
		}
		else {
			StringBuilder alias;
			bool is_forwarded_alias = false;
			if (node.moduleIdx < ~0u) {
				auto &module = parser.modules[node.moduleIdx];
				if (!kv.value.contains("$")) {
					if (is_node_name(parser, module.module_prefix, kv.value)) {
						alias.add(module.module_prefix);
					}
				} else {
					if (module.aliases.contains(kv.value)) {
						parser.modules.last().aliases.insert(
						    String().format("$%s", kv.key.begin()).begin(),
						    module.aliases[kv.value]);
					}
					is_forwarded_alias = true;
				}
			}
			if (!is_forwarded_alias) {
				alias.add(kv.value);
				parser.modules.last().aliases.insert(
				    String().format("$%s", kv.key.begin()).begin(), alias.build());
			}
		}
	}

	parser.modules.last().filename = path;
	parse_file_(parser, state.idx, path, uint32_t(parser.modules.size() - 1));
}

void Parser::parse_file(String filename)
{
	parse_file_(*this, ~0u, filename, ~0u);
}

void parse_inline_node(Parser &parser, ParserState &state, uint32_t moduleIdx)
{
	//TODO
}

StringView get_module_node_name(StringView name)
{
	size_t i = 0;
	if (!name.contains("/")) return StringView();
	for (;i < name.size() && name[i] != '/'; ++i);
	return StringView(name.begin(), i);
}

uint32_t get_module_depth(const String &name)
{
	uint32_t cnt = 0;
	for (char c : name) {
		if (c == '/') ++cnt;
	}
	return cnt;
}

StringView strip_first_module_name(StringView name)
{
	auto moduleName = get_module_node_name(name);
	return StringView(name.begin() + moduleName.size() + 1,
	                  name.size() - moduleName.size() - 1);
}

StringView strip_module_names(const String &name)
{
	StringView striped_name = name;
	while (striped_name.contains("/")) {
		striped_name = strip_first_module_name(striped_name);
	}

	return striped_name;
}

void add_node(Parser &           parser,
              const String &     name,
              const ParserState &state,
              const String &     type_string,
              uint32_t           moduleIdx)
{
	String node_name =
	    String().format("%s:%s", type_string.begin(), name.begin());

	Node new_node = {};
	// TODO value referencing doesn't properly work since we create a
	// new node everytime, so when we "override" a node, previous
	// parameters are lost. This fix here doesn't properly work but
	// instead breaks everything. Needs further investigation.

	// if (parser.nodeMap.contains(node_name)) {
	// 	new_node = parser.nodes[parser.nodeMap[node_name]];
	// }

	parser.nodeSet.insert(name);
	parser.nodeMap.insert(node_name, uint32_t(parser.nodes.size()));
	parser.nodes.add(new_node);
	auto &node        = parser.nodes.last();
	node.name         = name;
	node.parser_state = state;
	if (type_string != "Ignore" && type_string != "Include")
		node.type_string = type_string;
	node.moduleIdx = moduleIdx;
}

void init_kv_parser(KeyValuePair &kv)
{
	kv.value_parser.str = kv.value;
}

void parse_node(Parser &parser, ParserState &state, uint32_t moduleIdx)
{
	// snapshot of state for error messages in node type parsing
	// ParserState node_state = state;

	Token      node_type_token = parse_next_token(state);
	StringView node_type       = node_type_token.str;
	auto       colon           = parse_next_token(state);

	String node_name;
	if (colon.type == Token::Type::Colon) {
		node_name = String(parse_next_token(state).str);
		assert_token_type(
		    state, parse_next_token(state), Token::Type::CloseSquareBracket);
	}

	else {
		assert_token_type(state, colon, Token::Type::CloseSquareBracket);
	}

	StringBuilder module_prefix;
	if (moduleIdx < ~0u)
		module_prefix += parser.modules[moduleIdx].module_prefix;

	if (node_type == "Include") {
		String relative_module_prefix;
		if (!node_name.empty()) {
			relative_module_prefix.format("%s/", node_name.begin());
		}
		parser.modules.add(
		    {{},
		     "",
		     (StringBuilder(module_prefix) + relative_module_prefix).build(),
		     uint32_t(parser.nodes.size())});

		add_node(parser,
		         (StringBuilder(module_prefix) + node_name).build(),
		         state,
		         String(node_type),
		         moduleIdx);
		Node &moduleNode = parser.nodes.last();
		moduleNode.module_prefix = module_prefix.build();
		parse_key_values(state, moduleNode);
		parse_module_node(parser, state, moduleNode);
	} else {
		add_node(parser,
		         (StringBuilder(module_prefix) + node_name).build(),
		         state,
		         String(node_type),
		         moduleIdx);

		Node &node       = parser.nodes.last();
		node.module_prefix = module_prefix.build();
		node.is_override = node_name.contains("/");

		parse_key_values(state, parser.nodes.last());
	}
}

void parse_file_(Parser & parser,
                 uint32_t prev_state_idx,
                 String   filename,
                 uint32_t moduleIdx)
{
	if (parser.search_paths.empty()) parser.search_paths.add("./");

	std::ifstream graph_file;

	String path;
	if (is_relative_path(filename)) {
		for (const auto &sp : parser.search_paths) {
			auto filepath = (sp + filename).build();
			graph_file    = std::ifstream(filepath.begin(), std::ios_base::ate);
			if (!graph_file) {
				continue;
			} else {
				path = sp;
				break;
			}
		}
	}

	if (filename.contains("/")) {
		auto dir = extract_folder(filename);
		StringBuilder newpath;
		newpath.add(path);
		newpath.add("/");
		newpath.add(dir);
		parser.search_paths.add(newpath.build());
	}

	if (!graph_file) {
		String err = String().format("Graph file \"%s\" not found", filename.begin());
		if (prev_state_idx < ~0u) {
			report_error_and_exit(
			    report_parser_error(parser.parser_states[prev_state_idx], err));
		} else {
			report_error_and_exit(String().format(
			    "ERROR: Graph file \"%s\" not found", filename.begin()));
		}
	}

	size_t end = graph_file.tellg();
	graph_file.seekg(0);

	String file_content;
	file_content.resize(uint32_t(end + 1));
	graph_file.read(file_content.begin(), file_content.capacity());
	file_content.updateSize();

	StringView str(file_content.begin(), file_content.size());

	uint32_t idx = uint32_t(parser.parser_states.size());
	parser.parser_states.add(ParserState{
	    &parser, idx, prev_state_idx, moduleIdx, filename, str, 1, 1, 0});
	while (parser.parser_states[idx].i < parser.parser_states[idx].str.size()) {
		auto &state = parser.parser_states[idx];
		auto token = parse_next_token(state);
		switch (token.type) {
		case Token::Type::OpenSquareBracket: {
			parse_node(parser, state, moduleIdx);
			break;
		}
		default: break;
		}
	}
}

void parse_key_values(ParserState &state, Node &node)
{
	assert(state.i < state.str.size());
	for (; state.i < state.str.size() &&
	       peek_next_token(state).type != Token::Type::OpenSquareBracket;
	     advance(state)) {
		consume_all_ignorable(state);
		if (state.i >= state.str.size()) return;
		auto key = String(parse_next_token(state).str);
		node.settings.insert(key, {});
		node.settings.list.last().key = key;

		auto t = parse_next_token(state);
		assert_token_type(state, t, Token::Type::Equal);

		consume_all_ignorable(state);
		String value(strip_end(parse_until(state, ';')));
		node.settings.list.last().value = value;
		node.settings.list.last().value_parser =
		    ParserState{state.parser,
		                state.idx,
		                state.prev_state_idx,
		                state.moduleIdx,
		                state.filename,
		                {},
		                state.line,
		                state.col,
		                0};

		//TODO name resolution should really be a post process.
		auto          tmp_state = node.settings.list.last().value_parser;
		StringBuilder newStr;
		for (tmp_state.str = value; tmp_state.i < value.size();) {
			auto token = parse_next_token(tmp_state);
			String name(token.str);
			if (token.str.contains("$")) {
				name = resolve_alias(node.moduleIdx, token.str, tmp_state);
			}
			if (token.type == Token::Type::Name &&
			        is_node_name(
			            *state.parser, node.module_prefix, name) ||
			    token.str.startsWith("./")) {
				newStr.add(node.module_prefix);
			}
			newStr.add(name);
			if (tmp_state.i < value.size()) {
				if (peek_next_token(tmp_state).type == Token::Type::Colon) {
					newStr.add(":");
					skip_next_token(tmp_state);
				} else {
					newStr.add(" ");
				}
			}
		}
		node.settings.list.last().value = newStr.build();
	}
}
}  // namespace cfg
#endif
