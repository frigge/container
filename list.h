#pragma once

#include <cstdlib>
#include <stdexcept>
#include <algorithm>
#include <cassert>
#include "memory_helper.h"

template<typename T, size_t smallsize = frg::max_elements<T>(64)>
class Array
{
public:
	Array() :
	    data_(smallsize ? (T *)smallsize_buffer_ : nullptr),
	    size_(0),
	    capacity_(smallsize)
	{
	}

	Array(const Array &other) noexcept :
	    size_(other.size_),
	    capacity_(other.capacity_)
	{
		allocateMemory();
		copy_(other);
	}

	Array(Array &&other) noexcept :
	    data_(other.data_),
	    size_(other.size_),
	    capacity_(other.capacity_)
	{
		if (capacity_ && capacity_ <= smallsize) {
			data_ = (T*)smallsize_buffer_;
			move_(other);
		}
		other.data_     = nullptr;
		other.size_     = 0;
		other.capacity_ = 0;
	}

	~Array()
	{
		if (!data_) return;
		if (!capacity_) return;
		assert(size_ <= capacity_);
		for (size_t i = 0; i < size_; ++i) {
			data_[i].~T();
		}
		if (data_ && !is_small()) frg::aligned_free(data_);
		size_ = 0;
		capacity_ = 0;
		data_ = nullptr;
	}

	void reserve(size_t capacity)
	{
		if (capacity == capacity_) return;
		capacity_ = std::max(capacity, smallsize);
		moveDataToNewStorage_();
	}

	void resize(size_t size)
	{
		if (size == size_) return;
		size_t oldSize = size_;

		if (size > capacity_)
			reserve(size);

		if (size < oldSize) {
			for (size_t i = size; i < oldSize; ++i) {
				data_[i].~T();
			}
		}

		size_ = size;
		for (size_t i = oldSize; i < size_; ++i) {
			new (&data_[i]) T();
		}
	}

	Array& operator=(const Array &other)
	{
		this->~Array();

		size_     = other.size_;
		capacity_ = other.capacity_;
		allocateMemory();

		copy_(other);
		return *this;
	}

	T &      operator[](size_t idx) { return data_[idx]; }
	const T &operator[](size_t idx) const { return data_[idx]; }

	Array& operator=(Array &&other)
	{
		data_ = other.data_;
		size_ = other.size_;
		capacity_ = other.capacity_;

		if (capacity_ && capacity_ <= smallsize) {
			data_ = (T*)smallsize_buffer_;
			move_(other);
		}
		other.data_     = nullptr;
		other.size_     = 0;
		other.capacity_ = 0;
		return *this;
	}

	bool is_small() const
	{
		return smallsize && data_ >= (T *)&smallsize_buffer_[0] &&
		       data_ < (T *)&smallsize_buffer_[smallsize];
	}

	void clear()
	{
		for (size_t i = 0; i < size_; ++i) { data_[i].~T(); }

		size_ = 0;
		if (storage_frozen) return;

		if (!is_small()) frg::aligned_free(data_);
		data_ = smallsize ? (T*)&smallsize_buffer_[0] : nullptr;
		capacity_ = smallsize;
	}

	T *begin() { return data_; }
	T *end() { return data_ + size_; }

	const T *begin() const { return data_; }
	const T *end() const { return data_ + size_; }

	T& last()
	{
		return data_[size_ - 1];
	}

	const T& last() const
	{
		return data_[size_ - 1];
	}

	size_t size() const { return size_; }
	size_t capacity() const { return capacity_; }

	void add(T value)
	{
		if (size_ >= capacity_) {
			if (storage_frozen) {
				fprintf(stderr, "ERROR: array is full and not allowed to grow\n");
				assert(false);
				exit(EXIT_FAILURE);
			}
			auto oldCapacity = capacity_;
			bool was_small = is_small();
			if (!capacity_)
				capacity_ = 4;
			else
				capacity_ *= 2;
			T *oldData_ = data_;
			allocateMemory();
			for (size_t i = 0; i < size_; ++i) {
				auto *ptr = &data_[i];
				assert(&data_[size_] <= &data_[capacity_]);
				new (ptr) T(std::move(oldData_[i]));
				oldData_[i].~T();
			}
			if (!was_small)
				frg::aligned_free(oldData_);
		}

		auto *ptr = &data_[size_++];
		assert(&data_[size_] <= &data_[capacity_]);
		new (ptr) T(std::move(value));
	}

	void remove(size_t i)
	{
		size_t last = size_ - 1;
		if (last != i) {
			data_[i] = std::move(data_[last]);
		}
		
		size_--;

		if (storage_frozen) return;
		if (size_ <= capacity_ * 0.25) {
			auto oldCapacity = capacity_;
			capacity_ *= 0.5;
			capacity_ = std::max(capacity_, smallsize);
			if (capacity_ != oldCapacity)
				moveDataToNewStorage_();
		}
	}

	void insert(size_t idx, T value)
	{
		add(T());
		for(size_t i = idx; i < size_; ++i) {
			T tmp = std::move(data_[i]);
			data_[i] = value;
			value = std::move(tmp);
		}
	}

	bool empty() const
	{
		return !size_;
	}

	void freeze_storage(bool freeze)
	{
		storage_frozen = freeze;
	}

	bool take(T *data, size_t size)
	{
		clear();
		size_     = std::max(size, smallsize);
		capacity_ = size_;
		if (size <= smallsize) {
			for (size_t i = 0; i < size; ++i) {
				auto *ptr = &data_[i];
				assert(&data_[size_] <= &data_[capacity_]);
				new (ptr) T(std::move(data[i]));
				data[i].~T();
			}
			return false;
		}
		data_ = data;
		return true;
	}

protected:
	void allocateMemory()
	{
		if (smallsize && capacity_ <= smallsize) {
			data_ = (T*)smallsize_buffer_;
		} else {
			data_ = frg::allocate_elements<T>(capacity_);
		}
	}

	void moveDataToNewStorage_()
	{
		bool was_small = is_small();
		T *oldData = data_;
		allocateMemory();
		for (size_t i = 0; i < size_; ++i) {
			auto *ptr = &data_[i];
			assert(&data_[size_] <= &data_[capacity_]);
			new (ptr) T(std::move(oldData[i]));
			oldData[i].~T();
		}
		if (oldData && !was_small) frg::aligned_free(oldData);
	}

	void move_(Array& other)
	{
		for (size_t i = 0; i < size_; ++i) {
			auto *ptr = &data_[i];
			assert(&data_[size_] <= &data_[capacity_]);
			new (ptr) T(std::move(other.data_[i]));
		}
		other.data_     = nullptr;
		other.size_     = 0;
		other.capacity_ = 0;
	}

	void copy_(const Array& other)
	{
		for (size_t i = 0; i < size_; ++i) {
			auto *ptr = &data_[i];
			assert(&data_[size_] <= &data_[capacity_]);
			new (ptr) T(other.data_[i]);
		}
	}

	frg::uninitialized_t<T> smallsize_buffer_[smallsize];

	size_t size_{0};
	size_t capacity_{smallsize};
	T *    data_{nullptr};
	bool   storage_frozen{false};
};

template<typename T>
struct ArrayView
{
	template<size_t smallsize>
	ArrayView(const Array<T, smallsize> &arr) : data_(arr.begin()), size_(arr.size()) {}

	size_t size() const { return size_;}

	const T *begin() const { return data_; }
	const T *end() const { return data_ + size_; }

	const T &operator[](size_t idx) const { return data_[idx]; }

	const T *data_;
	size_t   size_;
};
