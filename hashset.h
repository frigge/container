#pragma once

#include "hash.h"
#include <cstdlib>
#include "memory_helper.h"

template<typename Key, size_t smallsize = frg::max_elements<Key>(64)>
class hashset
{
public:
	struct iterator
	{
		iterator operator++() {
			while (++pos < set_.capacity_ &&
			       set_.filled_[pos] != FILLED);
			return iterator{pos, set_};
		}

		Key& operator*()
		{
			return set_.keys_[pos];
		}

		bool operator!=(const iterator &other) const
		{
			return pos != other.pos;
		}

		bool filled() const
		{
			return set_.filled_[pos] == FILLED;
		}

		size_t pos{0};
		hashset<Key, smallsize> &set_;
	};

	hashset() :
	    filled_(smallsize ? filled_buffer : nullptr),
	    keys_(smallsize ? (Key *)keys_buffer : nullptr),
		capacity_(smallsize)
	{
		memset(filled_, 0, capacity_ * sizeof(Status));
	}

	~hashset()
	{
		if (keys_ && capacity_ > smallsize) {
			free_and_destruct_();
		}
	}

	iterator begin()
	{
		iterator ret{0, *this};
		if (ret.filled())
			return ret;
		else
			return ++ret;
	}

	iterator end() { return iterator{capacity_, *this}; }

	hashset(const hashset &other) :
	    size_(other.size_),
	    capacity_(other.capacity_)
	{
		if (capacity_ > smallsize) {
			filled_ = (Status *)malloc(capacity_ * sizeof(Status));
			keys_   = frg::allocate_elements<Key>(capacity_);
		} else {
			filled_ = filled_buffer;
			keys_   = (Key *)keys_buffer;
		}

		copy_(other);
	}

	hashset(hashset &&other) :
	    size_(other.size_),
	    capacity_(other.capacity_),
	    filled_(other.filled_),
	    keys_(other.keys_)
	{
		if (capacity_ <= smallsize) {
			keys_   = (Key *)keys_buffer;
			filled_ = filled_buffer;
			move_(std::move(other));
		}

		other.filled_ = nullptr;
		other.keys_   = nullptr;
	}

	hashset &operator=(const hashset &other)
	{
		~hashset();

		size_     = other.size_;
		capacity_ = other.capacity_;

		if (capacity_ > smallsize) {
			filled_ = (Status *)malloc(capacity_ * sizeof(Status));
			keys_   = frg::allocate_elements<Key>(capacity_);
		} else {
			filled_ = filled_buffer;
			keys_   = (Key *)keys_buffer;
		}

		copy_(other);

		return *this;
	}

	hashset &operator=(hashset &&other)
	{
		size_     = other.size_;
		capacity_ = other.capacity_;
		filled_   = other.filled_;
		keys_     = other.keys_;

		if (capacity_ <= smallsize) {
			filled_ = filled_buffer;
			keys_   = (Key *)keys_buffer;
			move_(std::move(other));
		}

		other.keys_   = nullptr;
		other.filled_ = nullptr;

		return *this;
	}

	void clear()
	{
		*this = hashset();
	}

	void insert(const Key &k)
	{
		if (size_ >= capacity_ / 2) {
			if (!capacity_)
				reserve(4);
			else
				reserve(capacity_ * 2);
		}

		auto idx = find(k);
		if (idx == ~0ul) {
			idx = find_empty(k);
			++size_;
		}
		assert(idx != ~0ul);
		
		new (&keys_[idx]) Key(k);

		filled_[idx] = FILLED;

	}

	size_t size() const { return size_; }
	size_t capacity() const { return capacity_; }

	bool contains(const Key &k) const
	{
		return find(k) != ~0ul;
	}

	void remove(const Key &k)
	{
		auto idx     = find(k);
		filled_[idx] = TOMBSTONE;
		keys_[idx].~Key();
		--size_;
		if (size_ < capacity_ / 4) {
			reserve(capacity_ / 2);
		}
	}

private:
	enum Status
	{
		EMPTY,
		TOMBSTONE,
		FILLED
	};

	size_t find(const Key &k) const
	{
		if (!capacity_) return ~0ul;
		size_t idx = get_idx(k);

		// linear probing
		while ((filled_[idx] == FILLED && keys_[idx] != k) ||
		       filled_[idx] == TOMBSTONE)
			idx = (++idx) % capacity_;

		if (filled_[idx] == FILLED && keys_[idx] == k) return idx;
		return ~0ul;
	}

	size_t get_idx(const Key &k) const
	{
		return frg_hash::hash<Key>::compute_hash(k) % capacity_;
	}

	size_t find_empty(const Key &k) const
	{
		size_t idx = get_idx(k);

		// linear probing
		while (filled_[idx] == FILLED) idx = (++idx) % capacity_;

		return idx;
	}

	void reserve(size_t newcapacity)
	{
		if (newcapacity <= std::max(capacity_, smallsize)) return;
		Key *   newkeys   = nullptr;
		Status *newfilled = nullptr;
		if (newcapacity > smallsize) {
            newfilled = (Status *)malloc(newcapacity * sizeof(Status));
			newkeys   = (Key *)frg::aligned_alloc(alignof(Key), newcapacity * sizeof(Key));
		} else {
			newkeys   = (Key *)keys_buffer;
			newfilled = filled_buffer;
		}
		memset(newfilled, 0, newcapacity * sizeof(Status));

		Key *   oldkeys   = keys_;
		Status *oldfilled = filled_;

		keys_   = newkeys;
		filled_ = newfilled;

		size_            = 0;
		auto oldcapacity = capacity_;
		capacity_        = newcapacity;
		for (size_t i = 0; i < oldcapacity; ++i) {
			if (oldfilled[i] == FILLED) { insert(oldkeys[i]); }
		}

		if (oldcapacity > smallsize) {
			keys_     = oldkeys;
			filled_   = oldfilled;
			capacity_ = oldcapacity;
			free_and_destruct_();
		}
		capacity_ = newcapacity;
		keys_     = newkeys;
		filled_   = newfilled;
	}

	void move_(hashset &&other)
	{
		memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
		for (size_t i = 0; i < capacity_; ++i) {
			if (other.filled_[i] == FILLED) {
				new (&keys_[i]) Key(std::move(other.keys_[i]));
			}
		}
	}

	void copy_(const hashset &other)
	{
		memcpy(filled_, other.filled_, capacity_ * sizeof(Status));
		for (size_t i = 0; i < capacity_; ++i) {
			if (other.filled_[i] == FILLED) {
				new (&keys_[i]) Key(other.keys_[i]);
			}
		}
	}

	void free_and_destruct_()
	{
		for (size_t i = 0; i < capacity_; ++i) {
			if (filled_[i] == FILLED) {
				keys_[i].~Key();
			}
		}
		free(filled_);
		frg::aligned_free(keys_);

		filled_ = nullptr;
		keys_   = nullptr;
	}

//members
	frg::uninitialized_t<Key> keys_buffer[smallsize];
	Status  filled_buffer[smallsize];
	Status *filled_;
	Key *   keys_;

	size_t  size_{0};
	size_t  capacity_{smallsize};
};
