#pragma once

#include "list.h"

template<typename T, int smallsize = 4>
class Stack : public Array<T, smallsize>
{
public:
	using Array<T, smallsize>::Array;

	void push(T item)
	{
		this->add(item);
	}

	T pop()
	{
		size_t last = this->size_ - 1;
		T item = this->data_[last];
		this->remove(last);
		return item;
	}
};
