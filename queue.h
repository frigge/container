#pragma once

#include "list.h"

template<typename T, int smallsize = 4>
class Queue
{
public:
	void push(T item)
	{
		list.add(item);
	}

	T pop()
	{
		T item = list[first];
		list[first] = T();
		++first;
		if (first >= 16) {
			Array<T, smallsize> newlist;
			newlist.reserve(size());
			for (int i = 0; i < newlist.size(); ++i) {
				newlist[i] = list[first + i];
			}
			list  = newlist;
			first = 0;
		}
		return item;
	}

	bool empty() { return list.size() == first; }

	size_t size() { return list.size() - first; }

private:
	Array<T, smallsize> list;
	size_t first = 0;
};
