#pragma once
#include <cstdint>
#include <cfloat>
#include <cmath>
#include <algorithm>

struct Vec2i;
struct Vec2f
{
	Vec2f() : x(0), y(0) {}
	Vec2f(float x) : x(x), y(x) {}
	Vec2f(float x, float y) : x(x), y(y) {}

	inline Vec2f& operator+=(const Vec2f &other);
	inline Vec2f& operator-=(const Vec2f &other);
	inline Vec2f& operator*=(const Vec2f &other);
	inline Vec2f& operator/=(const Vec2f &other);
	inline Vec2f& operator*=(const float other);
	inline Vec2f& operator/=(const float other);
	inline bool operator==(const Vec2f &other);
	inline bool operator!=(const Vec2f &other);
	inline operator Vec2i() const;

	float x;
	float y;
};

inline float absf(float val)
{
	return (float)(~(1 << 31) & (int32_t)val);
}

inline Vec2f abs(const Vec2f &vec) { return {absf(vec.x), absf(vec.y)}; }

struct Vec2i
{
	int32_t x;
	int32_t y;

	inline operator Vec2f() const { return {float(x), float(y)}; }
};

inline Vec2f::operator Vec2i() const { return {int32_t(x), int32_t(y)}; }

struct Vec3f;
struct Vec3i
{
	int32_t x;
	int32_t y;
	int32_t z;

	inline operator Vec3f() const;
};

struct Vec3f
{
	inline Vec3f& operator+=(const Vec3f other);
	inline Vec3f& operator-=(const Vec3f &other);
	inline Vec3f& operator*=(const Vec3f &other);
	inline Vec3f& operator/=(const Vec3f &other);
	inline Vec3f& operator*=(const float other);
	inline Vec3f& operator/=(const float other);

	inline float& operator[](int idx) { return data[idx]; }
	inline Vec3f &operator*(float value)
	{
		x *= value;
		y *= value;
		z *= value;
		return *this;
	}

	inline Vec3f &operator*(int value)
	{
		x *= value;
		y *= value;
		z *= value;
		return *this;
	}

	union {
		struct
		{
			float x;
			float y;
			float z;
		};
		float data[3];
	};
};

inline Vec3f operator-(Vec3f vec)
{
	return Vec3f{-vec.x, -vec.y, -vec.z};
}

inline float dot(const Vec3f &a, const Vec3f &b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vec3i::operator Vec3f() const { return {float(x), float(y), float(z)}; }

inline float dot(const Vec2f &a, const Vec2f &b)
{
	return a.x * b.x + a.y * b.y;
}

inline float len(const Vec2f &vec)
{
	return sqrtf(dot(vec, vec));
}

struct Col4f
{
	float r;
	float g;
	float b;
	float a;
};

inline Vec3f operator+(const Vec3f &a, const Vec3f &b)
{
	return {a.x + b.x, a.y + b.y, a.z + b.z};
}

inline Vec2f operator+(const Vec2f &a, const Vec2f &b)
{
	return {a.x + b.x, a.y + b.y};
}

inline Vec3f operator-(const Vec3f &a, const Vec3f &b)
{
	return {a.x - b.x, a.y - b.y, a.z - b.z};
}

inline Vec2f operator-(const Vec2f &a, const Vec2f &b)
{
	return {a.x - b.x, a.y - b.y};
}

inline Vec3f operator*(const Vec3f &a, const Vec3f &b)
{
	return {a.x * b.x, a.y * b.y, a.z * b.z};
}

inline Vec2f operator*(const Vec2f &a, const Vec2f &b)
{
	return {a.x * b.x, a.y * b.y};
}

inline Vec3f operator/(const Vec3f &a, const Vec3f &b)
{
	return {a.x / b.x, a.y / b.y, a.z / b.z};
}

inline Vec2f operator/(const Vec2f &a, const Vec2f &b)
{
	return {a.x / b.x, a.y / b.y};
}

inline Vec3f operator*(const Vec3f &a, const float b)
{
	return {a.x * b, a.y * b, a.z * b};
}

inline Vec2f operator*(const Vec2f &a, const float b)
{
	return {a.x * b, a.y * b};
}

inline Vec3f operator/(const Vec3f &a, const float b)
{
	return {a.x / b, a.y / b, a.z / b};
}

inline Vec2f operator/(const Vec2f &a, const float b)
{
	return {a.x / b, a.y / b};
}

inline Vec3f operator*(const float b, const Vec3f &a)
{
	return {a.x * b, a.y * b, a.z * b};
}

inline Vec2f operator*(const float b, const Vec2f &a)
{
	return {a.x * b, a.y * b};
}

inline Vec3f operator/(const float a, const Vec3f &b)
{
	return {a / b.x, a / b.y, a / b.z};
}

inline Vec2f operator/(const float a, const Vec2f &b)
{
	return {a / b.x, a / b.y};
}

inline Vec2f& Vec2f::operator+=(const Vec2f &other)
{
	*this = *this + other;
	return *this;
}

inline Vec2f& Vec2f::operator-=(const Vec2f &other)
{
	*this = *this - other;
	return *this;
}

inline Vec2f& Vec2f::operator*=(const Vec2f &other)
{
	*this = *this * other;
	return *this;
}

inline Vec2f& Vec2f::operator/=(const Vec2f &other)
{
	*this = *this / other;
	return *this;
}

inline Vec2f& Vec2f::operator*=(const float other)
{
	*this = *this * other;
	return *this;
}

inline Vec2f& Vec2f::operator/=(const float other)
{
	*this = *this / other;
	return *this;
}

inline bool Vec2f::operator==(const Vec2f &other)
{
	return x == other.x && y == other.y;
}

inline bool Vec2f::operator!=(const Vec2f &other)
{
	return !(*this == other);
}

inline Vec3f& Vec3f::operator+=(const Vec3f other)
{
	*this = *this + other;
	return *this;
}

inline Vec3f& Vec3f::operator-=(const Vec3f &other)
{
	*this = *this - other;
	return *this;
}

inline Vec3f& Vec3f::operator*=(const Vec3f &other)
{
	*this = *this * other;
	return *this;
}

inline Vec3f& Vec3f::operator/=(const Vec3f &other)
{
	*this = *this / other;
	return *this;
}

inline Vec3f& Vec3f::operator*=(const float other)
{
	*this = *this * other;
	return *this;
}

inline Vec3f& Vec3f::operator/=(const float other)
{
	*this = *this / other;
	return *this;
}

struct BBox
{
	inline BBox& operator|=(const BBox &other);
	inline BBox& operator|=(const Vec3f &v);

	Vec3f bbmin{FLT_MAX};
	Vec3f bbmax{-FLT_MAX};
};

inline BBox operator|(const BBox &a, const BBox &b)
{
	BBox ret;
	ret.bbmin.x = std::min(a.bbmin.x, b.bbmin.x);
	ret.bbmin.y = std::min(a.bbmin.y, b.bbmin.y);
	ret.bbmin.z = std::min(a.bbmin.z, b.bbmin.z);
	ret.bbmax.x = std::max(a.bbmax.x, b.bbmax.x);
	ret.bbmax.y = std::max(a.bbmax.y, b.bbmax.y);
	ret.bbmax.z = std::max(a.bbmax.z, b.bbmax.z);
	return ret;
}

inline BBox operator|(const BBox &a, const Vec3f &v)
{
	BBox ret;
	ret.bbmin.x = std::min(a.bbmin.x, v.x);
	ret.bbmin.y = std::min(a.bbmin.y, v.y);
	ret.bbmin.z = std::min(a.bbmin.z, v.z);
	ret.bbmax.x = std::max(a.bbmax.x, v.x);
	ret.bbmax.y = std::max(a.bbmax.y, v.y);
	ret.bbmax.z = std::max(a.bbmax.z, v.z);
	return ret;
}

inline BBox& BBox::operator|=(const BBox &other)
{
	*this = *this | other;
	return *this;
}

inline BBox& BBox::operator|=(const Vec3f &v)
{
	*this = *this | v;
	return *this;
}
