#pragma once

#ifdef linux
#include <cxxabi.h>
#endif

#include "str.h"

class Type
{
public:
	template<typename T>
	static Type create()
	{
		auto &tid = typeid(T);
#ifdef linux
		int   status;
		auto *name = abi::__cxa_demangle(tid.name(), 0, 0, &status);
#else
		auto name = tid.name();
#endif
		return Type(name, tid.hash_code());
	}

	Type() : name_("undefined"), hash_(~0u) {}

	String name() const { return name_; }
	size_t      hash() const { return hash_; }

private:
	Type(const String name, const size_t hash) : name_(name), hash_(hash)
	{
	}

	String name_;
	size_t hash_;
};

inline bool operator==(const Type &a, const Type &b)
{
	return a.hash() == b.hash();
}

inline bool operator!=(const Type &a, const Type &b)
{
	return a.hash() != b.hash();
}

#define ANY_SMALLSIZE 64
class Any
{
	struct AnyBase
	{
		Type t;
		AnyBase(Type t) :
		    t(t)
		{
		}

		virtual ~AnyBase() {}

		virtual AnyBase* clone(void *mem) = 0;
	};

	template<typename T>
	struct AnyData : public AnyBase
	{
		T value;
		AnyData(T val) :
		    AnyBase(Type::create<T>()),
		    value(val)
		{
		}

		AnyBase *clone(void *mem=nullptr)
		{
			if (mem) {
				return new (mem) AnyData<T>(value);
			} else {
				return new AnyData<T>(value);
			}
		}
	};

public:
	Any() : data(nullptr) {}

	template<typename T>
	Any(T val)
	{
		auto * storage      = (void *)mem;
		size_t storage_size = ANY_SMALLSIZE;

		if (std::align(alignof(AnyData<T>),
		               sizeof(AnyData<T>),
		               storage,
		               storage_size)) {
			data = new (storage) AnyData<T>(val);
		} else {
			data = new AnyData<T>(val);
		}
	}

	Any(const Any &other) : data(nullptr)
	{
		if (other.data) {
			if ((void *)other.data != (void *)other.mem)
				data = other.data->clone(nullptr);
			else
				data = other.data->clone(mem);
		}
	}

	Any(Any &&other)
	{
		if (other.data) {
			if ((void *)other.data != (void *)other.mem)
				data = other.data->clone(nullptr);
			else
				data = other.data->clone(mem);
		}
	}

	~Any()
	{
		destroy();
	}

	void destroy()
	{
		if (data) {
			if ((void *)data != (void *)mem)
				delete data;
			else {
				data->~AnyBase();
			}
		}
	}
	  

	Any &operator=(const Any &other)
	{
		destroy();

		if (other.data) {
			if ((void *)other.data != (void *)other.mem)
				data = other.data->clone(nullptr);
			else
				data = other.data->clone(mem);
		}
		return *this;
	}

	Any &operator=(Any &&other)
	{
		destroy();

		if (other.data) {
			if ((void *)other.data != (void *)other.mem) {
				data = other.data->clone(nullptr);
			} else {
				data = other.data->clone(mem);
			}
		}
		return *this;
	}

	bool empty() const { return !data; }

	Type type() const
	{
		if (data)
			return data->t;
		else
			return Type();
	}

private:
	AnyBase *data{nullptr};
	char mem[ANY_SMALLSIZE];

	template<typename T>
	friend T& cast(const Any& any);

	template<typename T>
	friend bool isType(const Any& any);
};

template<typename T>
T& cast(const Any& any)
{
	auto &tid = typeid(T);
	if (any.data->t.hash() != tid.hash_code()) {
#ifdef linux
		int status;
		auto *name = abi::__cxa_demangle(tid.name(), 0, 0, &status);
#else
		auto name = tid.name();
#endif
		printf("bad any cast. Tried to cast %s to %s\n",
		       any.data->t.name().begin(),
		       name);
		assert(false);
		exit(EXIT_FAILURE);
	}

	return static_cast<Any::AnyData<T>*>(any.data)->value;
}

template<typename T>
bool isType(const Any& any)
{
	return typeid(T).hash_code() == any.data->t.hash();
}
