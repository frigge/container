#include "vec.h"

struct Rect
{
	Rect  operator*(Vec2f scale) { return {tl * scale, br * scale}; }
	Rect  operator/(Vec2f scale) { return {tl / scale, br / scale}; }
	Rect  operator-(Vec2f offset) { return {tl - offset, br - offset}; }
	Rect  operator+(Vec2f offset) { return {tl + offset, br + offset}; }
	Rect  operator|(const Rect &r)
	{
		return {{std::min(r.tl.x, tl.x), std::min(r.tl.y, tl.y)},
		        {std::max(r.br.x, br.x), std::max(r.br.y, br.y)}};
	}

	Rect &operator*=(Vec2f scale)
	{
		*this = *this * scale;
		return *this;
	}

	Rect &operator/=(Vec2f scale)
	{
		*this = *this / scale;
		return *this;
	}

	Rect &operator+=(Vec2f offset)
	{
		*this = *this + offset;
		return *this;
	}

	Rect &operator-=(Vec2f offset)
	{
		*this = *this - offset;
		return *this;
	}

	Rect &operator|=(const Rect &r)
	{
		*this = *this | r;
		return *this;
	}

	Vec2f size() const { return br - tl; }

	float area() const
	{
		Vec2f s = size();
		return s.x * s.y;
	}

	float aspect() const
	{
		Vec2f s = size();
		return s.x / s.y;
	}

	Vec2f center() const
	{
		return (tl + br) * 0.5f;
	}

	Vec2f tl;
	Vec2f br;
};
