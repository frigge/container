#pragma once
#include "hashmap.h"
#include "list.h"

template<typename Key, typename Value>
struct ordered_hashmap
{
	void insert(const Key &k, const Value &v)
	{
		bool contains = map.contains(k);
		map.insert(k, list.size());
		if (!contains) {
			list.add(v);
			keys.add(k);
		} else {
			list[map[k]] = v;
		}
	}

	void insert(const Key &k, Value &&v)
	{
		bool contains = map.contains(k);
		map.insert(k, list.size());
		if (!contains) {
			list.add(std::forward<Value>(v));
			keys.add(k);
		} else {
			list[map[k]] = std::move(v);
		}
	}

	void clear()
	{
		map.clear();
		list.clear();
	}

	Value &get(const Key &key)
	{
		return list[map[key]];
	}

	const Value &get(const Key &key) const
	{
		return list[map[key]];
	}

	Value &sub(uint32_t idx)
	{
		return list[idx];
	}

	const Value &sub(uint32_t idx) const
	{
		return list[idx];
	}

	bool contains(const Key &key) const
	{
		return map.contains(key);
	}

	size_t size() const
	{
		return list.size();
	}

	Array<Value>           list;
	Array<Key>             keys;
	hashmap<Key, uint32_t> map;
};
