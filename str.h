#pragma once

#include "hash.h"
#include "list.h"
#include <functional>

#ifdef WIN32
#include <intrin.h>
#endif

class String;

class StringView
{
public:
	StringView() = default;

	StringView(const char* str, size_t size) : str_(str), size_(size) {}
	explicit StringView(const char* str) : str_(str), size_(strlen(str)) {}

	const char *begin() const
	{
		return str_;
	}

	bool startsWith(const char *prefix, size_t length = ~0ul) const
	{
		if (length == ~0u) length = strlen(prefix);
		if (length > size_) return false;

		return StringView(str_, length) == StringView(prefix, length);
	}

	bool startsWith(const StringView &prefix) const
	{
		return startsWith(prefix.str_, prefix.size_);
	}

	bool endsWith(const char *suffix, size_t length = ~0ul) const
	{
		if (length == ~0u) length = strlen(suffix);
		if (length > size_) return false;
		return StringView(str_ + (size_ - length), length) ==
		       StringView(suffix, length);
	}

	bool endsWith(const StringView &suffix) const
	{
		return endsWith(suffix.str_, suffix.size_);
	}

	size_t find(const char *substr, size_t substr_length = ~0ul) const
	{
		if (substr_length == ~0ul) substr_length = strlen(substr);
		size_t substr_start = ~0ul;
		if (substr_length > size_) return ~0ul;

		for (size_t i = 0; i < (size_ - substr_length + 1); ++i) {
			if (str_[i] == substr[0]) {
				substr_start = i;
				for (size_t j = 0; j < substr_length; ++j) {
					if (j == (substr_length - 1) && substr[j] == str_[i + j])
						return substr_start;
					if (substr[j] != str_[i + j]) break;
				}
			}
		}

		return ~0ul;
	}

	size_t find(const StringView &substr) const
	{
		return find(substr.str_, substr.size_);
	}

	bool contains(const char *substr, size_t length = ~0ul) const
	{
		return find(substr, length) < size_;
	}

	bool contains(const StringView &substr) const
	{
		return contains(substr.str_, substr.size_);
	}

	bool operator==(const char *other) const
	{
		auto othersize = strlen(other);
		if (othersize != size_) return false;
		for (size_t i = 0; i < size_; ++i) {
			if (other[i] != str_[i]) return false;
		}
		return true;
	}
	bool operator==(const StringView &other) const
	{
		if (other.size_ != size_) return false;
		for (size_t i = 0; i < size_; ++i) {
			if (other[i] != str_[i]) return false;
		}
		return true;
	}

	bool operator!=(const char *other) const
	{
		return !(*this == other);
	}

	bool operator!=(const StringView &other) const
	{
		return !(*this == other);
	}

	const char& operator[](size_t i) const
	{
		return str_[i];
	}

	size_t size() const { return size_; }


private:
	const char *str_{nullptr};
	size_t      size_{0};
};

class String : public Array<char>
{
public:
	using Array<char>::Array;

	String() : Array<char>::Array() { zero_(); }

	explicit String(const StringView &str) : Array<char>::Array()
	{
		zero_();
		size_ = str.size();
		if (!size_) return;
		if (capacity_ < (size_ + 1)) {
#ifdef _MSC_VER
			capacity_ = size_t(1) << (size_t(64) - __lzcnt64(size_));
#else
			capacity_ = 1 << (64 - __builtin_clzl(size_));
#endif
			data_ = (char *)frg::allocate_elements<char>(capacity_);
			zero_();
		}
		memcpy(data_, str.begin(), size_);
	}

	String(const char *str) : Array<char>::Array()
	{
		zero_();
		size_ = strlen(str);
		if (capacity_ < (size_ + 1)) {
#ifdef _MSC_VER
			capacity_ = size_t(1) << (size_t(64) - __lzcnt64(size_));
#else
			capacity_ = 1 << (64 - __builtin_clzl(size_));
#endif
			data_ = (char *)frg::allocate_elements<char>(capacity_);
			zero_();
		}
		strcpy(data_, str);
	}

	String(const String &other) : Array<char>(other)
	{
		if (capacity_ > size_) memset(data_ + size_, '\0', (capacity_ - size_));
	}

	String(String &&other) : Array<char>(std::move(other))
	{
		if (capacity_ > size_) memset(data_ + size_, '\0', (capacity_ - size_));
	}

	String &operator=(const String &other)
	{
		this->~String();
		size_ = other.size_;
		capacity_ = other.capacity_;
		allocateMemory();
		zero_();
		copy_(other);
		return *this;
	}

	String &operator=(String &&other)
	{
		data_ = other.data_;
		size_ = other.size_;
		capacity_ = other.capacity_;

		if (capacity_ == 64) {
			data_ = (char*)smallsize_buffer_;
			move_(other);
		}

		other.data_     = nullptr;
		other.size_     = 0;
		other.capacity_ = 0;
		return *this;
	}

	template<typename ... Args>
	String& format(const char *format_str, Args ... args)
	{
		int necessary_bytes = snprintf(data_, capacity_, format_str, args...) + 1;
		if (necessary_bytes > capacity_) {
			resize(necessary_bytes);
			snprintf(data_, capacity_, format_str, args...);
		}
		updateSize();
		return *this;
	}

	void updateSize()
	{
		size_ = strlen(data_);
	}

	void resize(uint32_t size)
	{
		if (size <= 64) return;
		size_t oldCapacity = capacity_;
		Array<char>::reserve(size);
		size_ = size;
		data_[size_ - 1] = '\0';
		if (oldCapacity < capacity_) {
			memset(data_ + oldCapacity, '\0', capacity_ - oldCapacity);
		}
	}

	void reserve(uint32_t size)
	{
		resize(size);
	}

	bool operator==(const char *other) const
	{
		return StringView(data_, size_) == other;
	}

	bool operator!=(const char *other) const
	{
		return !(*this == other);
	}

	bool operator==(const String &other) const
	{
		return *this == other.data_;
	}

	bool operator!=(const String &other) const
	{
		return !(*this == other);
	}

	operator StringView() const
	{
		return StringView(data_, size_);
	}

	bool contains(const char *substr) const
	{
		return StringView(data_, size_).contains(substr);
	}

	bool contains(const StringView &substr) const
	{
		return StringView(data_, size_).contains(substr);
	}

	bool startsWith(const char *prefix) const
	{
		size_t prefix_size = strlen(prefix);
		return StringView(data_, size_).startsWith(prefix, prefix_size);
	}

	bool startsWith(const StringView &prefix) const
	{
		return StringView(data_, size_).startsWith(prefix);
	}

	bool endsWith(const char *suffix) const
	{
		size_t suffix_size = strlen(suffix);
		return StringView(data_, size_).endsWith(suffix, suffix_size);
	}

	bool endsWith(const StringView &suffix) const
	{
		return StringView(data_, size_).endsWith(suffix);
	}

	String replace(const char* substr, const char* replacement)
	{
		return replace(StringView(substr), StringView(replacement));
	}

	inline String replace(StringView substr, StringView replacement);
	
	size_t size() const { return size_; }

	void zero()
	{
		zero_();
		size_ = 0;
	}

private:
	void zero_()
	{
		memset(data_, '\0', capacity_);
	}

};

class StringBuilder
{
public:
	StringBuilder& add(const String &str)
	{
		stringList.add(str);
		elementType.add(STRING);
		final_size += str.size();
		return *this;
	}

	StringBuilder& add(const StringView &str)
	{
		stringViewList.add(str);
		elementType.add(VIEW);
		final_size += str.size();
		return *this;
	}

	StringBuilder &operator+=(const String &str)
	{
		return add(str);
	}

	StringBuilder& add(const StringBuilder &builder)
	{
		for (uint32_t i = 0, strIdx = 0, viewIdx = 0; i < builder.elementType.size();
		     ++i) {
			switch (elementType[i]) {
			case STRING:
			{
				const auto &str = builder.stringList[strIdx++];
				add(str);
				break;
			}
			case VIEW:
			{
				const auto &str = builder.stringViewList[viewIdx++];
				add(str);
				break;
			}
			}
		}
		return *this;
	}

	StringBuilder &operator+=(const StringBuilder &builder)
	{
		return add(builder);
	}

	StringBuilder &operator+=(const StringView &str)
	{
		add(str);
		return *this;
	}

	String build()
	{
		String ret;
		ret.resize(uint32_t(final_size + 1));
		size_t offset{0};

		for (uint32_t i = 0, strIdx = 0, viewIdx = 0; i < elementType.size();
		     ++i) {
			switch (elementType[i]) {
			case STRING:
			{
				const auto &str = stringList[strIdx++];
				memcpy(ret.begin() + offset, str.begin(), str.size());
				offset += str.size();
				break;
			}
			case VIEW:
			{
				const auto &str = stringViewList[viewIdx++];
				memcpy(ret.begin() + offset, str.begin(), str.size());
				offset += str.size();
				break;
			}
			}
		}
		ret[offset] = '\0';
		ret.updateSize();
		return ret;
	}

private:
	enum BuilderElementType
	{
		STRING,
		VIEW,
	};

	Array<String, 4>          stringList;
	Array<StringView, 4>      stringViewList;
	Array<BuilderElementType> elementType;
	size_t                    final_size{0};
};

inline String String::replace(StringView substr, StringView replacement)
{
	if (!contains(substr.begin())) { return *this; }

	char *        view_start = data_;
	size_t        view_size  = size_;
	StringView    view(view_start, view_size);
	StringBuilder ret;
	for (;;) {
		size_t match_pos = view.find(substr);
		if (match_pos >= view.size()) break;

		ret.add(StringView(view.begin(), match_pos));
		ret.add(replacement);

		view_start += match_pos + substr.size();
		view_size -= match_pos + substr.size();
		view = StringView(view_start, view_size);
	}
	ret.add(view);

	return ret.build();
}

inline StringBuilder operator+(const String &left, const String &right)
{
	StringBuilder ret;
	ret += left;
	ret += right;
	return ret;
}

inline StringBuilder operator+(const StringView &left, const StringView &right)
{
	StringBuilder ret;
	ret += left;
	ret += right;
	return ret;
}

inline StringBuilder operator+(const StringView &left, const String &right)
{
	StringBuilder ret;
	ret += left;
	ret += right;
	return ret;
}

inline StringBuilder operator+(const String &left, const StringView &right)
{
	StringBuilder ret;
	ret += left;
	ret += right;
	return ret;
}

inline StringBuilder operator+(StringBuilder &&builder, const String &right)
{
	builder.add(right);
	return builder;
}

inline StringBuilder operator+(StringBuilder &&builder, const StringView &right)
{
	builder.add(right);
	return builder;
}

inline StringBuilder operator+(const StringBuilder &builder, const String &right)
{
	StringBuilder ret;
	ret.add(right);
	return ret;
}

inline StringBuilder operator+(const StringBuilder &builder, const StringView &right)
{
	StringBuilder ret;
	ret.add(right);
	return ret;
}

inline StringBuilder operator+(const StringBuilder &left, StringBuilder &right)
{
	StringBuilder ret;
	ret.add(left);
	ret.add(right);
	return ret;
}

namespace frg_hash
{
template<>
struct hash<String>
{
	static uint64_t compute_hash(const String &str)
	{
		return hash<const char *>::compute_hash(str.begin());
	}
};

template<>
struct hash<StringView>
{
	static uint64_t compute_hash(const StringView &str)
	{
		return hash<String>::compute_hash(String(str));
	}
};

}  // namespace frg_hash
