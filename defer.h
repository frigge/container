#pragma once

template<typename T>
class Deferred
{
public:
	explicit Deferred(T act) : act_(act) {}
	~Deferred() { act_(); }

private:
	T act_;
};
